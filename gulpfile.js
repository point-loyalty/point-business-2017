// ////////////////////////////////////////////
// Required Modules
// ////////////////////////////////////////////
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    pump = require('pump'),
    minify = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
      reload = browserSync.reload,
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require('gulp-autoprefixer');



// ////////////////////////////////////////////
// Scripts
// ////////////////////////////////////////////
gulp.task('scripts', function (cb) {
  pump([
    gulp.src([
      'bower_components/jquery/dist/jquery.js',
      'src/js/vendor/pace.min.js',
      'bower_components/what-input/dist/what-input.js',
      'src/js/vendor/select2.min.js',
      'src/js/vendor/jquery.auto-complete.js',
      'bower_components/foundation-sites/dist/js/foundation.js',
  // Nothing below
      'src/js/includes.js',
      'src/js/app.js'
    ]),
    concat('core.js'),
    //uglify(),
    //jshint(),
    //jshint.reporter('jshint-stylish'),
    gulp.dest('dist/js'),
    gulp.dest('src/js'),
  ],
  cb
  );
});



gulp.task('scripts:lone', function() {
  gulp.src([
    'src/js/*.js',
    '!src/js/includes.js',
    '!src/js/app.js',
    '!src/js/core.js',
    '!src/js/core.min.js'
  ])
  .pipe(plumber())
  .pipe(gulp.dest('dist/js/'))
  .pipe(reload({stream:true}));
});





// ////////////////////////////////////////////
// Sass Tasks
// ////////////////////////////////////////////
gulp.task('sass', function(){
  return gulp.src(['src/scss/core.scss'])
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(sass({
    includePaths: ['bower_components/foundation-sites/scss']
  }).on('error', sass.logError))
  .pipe(autoprefixer('last 2 versions', 'ie >= 9', 'and_chr >= 2.3'))
  .pipe(gulp.dest('src/css'))
  .pipe(minify())
  .pipe(rename({suffix:'.min'}))
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('dist/css'))
  .pipe(gulp.dest('src/css'))
  .pipe(reload({stream:true}));
});




// ////////////////////////////////////////////
// HTML/PHP Tasks
// ////////////////////////////////////////////
gulp.task('html', function(){
  gulp.src('src/**/*.+(html|php|json)')
  .pipe(gulp.dest('dist'))
  .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////
// Image Optimization
// ////////////////////////////////////////////
gulp.task('img', function(){
  gulp.src('src/img/**/*')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/img'));
});


// ////////////////////////////////////////////
// Misc
// ////////////////////////////////////////////
gulp.task('misc', function(){
  gulp.src('src/includes/**/*')
  .pipe(gulp.dest('dist/includes/'));
})


// ////////////////////////////////////////////
// BrowserSync Config
// ////////////////////////////////////////////
gulp.task('browser-sync', function(){
  browserSync({
    proxy: "http://localhost:80/",
    files: ["**/*.php"]
  });
});



// ////////////////////////////////////////////
// Watch Task (Handle JS, SCSS, IMG, Reload)
// ////////////////////////////////////////////
gulp.task('watch', function(){
  gulp.watch(['src/js/app.js', 'src/js/includes.js'], ['scripts']);
  gulp.watch(['src/js/*.js', '!src/js/app.js', '!src/js/includes.js'], ['scripts:lone']);
  gulp.watch('src/scss/**/*.+(scss|css)', ['sass']);
  gulp.watch('src/img/**/*', ['img']);
  gulp.watch('src/**/*.+(php|html|json)', ['html']);
  gulp.watch('src/includes/**/*', ['misc']);
});



// ////////////////////////////////////////////
// BUILD Functions
// ////////////////////////////////////////////
gulp.task('build:clean', function() {
  return del([
    'dist/**'
  ]);
});

gulp.task('build', ['scripts', 'scripts:lone', 'sass' , 'img', 'html', 'misc']);


// ////////////////////////////////////////////
// Default Task
// ////////////////////////////////////////////
gulp.task('default', ['scripts', 'scripts:lone', 'sass', 'img', 'html', 'misc', 'browser-sync', 'watch']);
