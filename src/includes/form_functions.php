<?php // Input Field ?>
<?php function formItem($label = NULL, $id, $labelText, $placeholder, $type, $required, $error = NULL, $count = NULL, $max) { ?>
  <?php $labelClass = is_null($label) ? "nolabel" : ""; ?>
  <!-- ••• <?php echo $id; ?> ••• -->
  <div class="input-field <?php echo $labelClass; ?>">
    <label class="<?php if ($required === TRUE) { ?>req <?php }; ?>" for="<?php echo $id; ?>"><?php echo $labelText; ?></label>
    <input id="<?php echo $id; ?>" name="<?php echo $id; ?>" type="<?php echo $type; ?>" aria-describedby="Enter <?php echo $id; ?> here" placeholder="<?php echo $placeholder; ?>" <?php if ($required == TRUE) :?> required required="required" <?php endif; ?> pattern="<?php echo $type; ?>" <?php if (!is_null($count)) { ?>maxlength="<?php echo $max; ?>"<?php } ?>/>
    <span class="bar"></span>
    <?php if (!is_null($count)) { ?><div class="form_count" id="chars-<?php echo $id; ?>"></div><?php } ?>
    <span class="form-error">
      <?php $errorMessage = is_null($error) ? "This is an error in the field" : $error; ?>
      <?php echo $errorMessage; ?>
    </span>
  </div>
<?php } ?>

<?php // Textarea ?>
<?php function formTextarea($label, $id, $labelText, $required, $error, $count, $max) { ?>
  <!-- ••• <?php echo $id; ?> ••• -->
  <div class="input-field <?php if (is_null($label)) :?>nolabel<?php endif; ?>">
    <label class="<?php if ($required === TRUE) { ?>req <?php }; ?>" for="<?php echo $id; ?>">
      <?php echo $labelText; ?>
    </label>
    <textarea id="<?php echo $id; ?>"<?php if ($required == TRUE) :?> name="<?php echo $id; ?>" required required="required" <?php endif; ?> <?php if (!is_null($count)) { ?>maxlength="<?php echo $max; ?>"<?php } ?> ></textarea>
    <span class="bar"></span>

      <?php if (!is_null($count)) { ?><div class="form_count" id="chars-<?php echo $id; ?>"></div><?php } ?>
      <span class="form-error">
        <?php $errorMessage = is_null($error) ? "This is an error in this field" : $error; ?>
        <?php echo $errorMessage; ?>
      </span>

  </div>
<?php } ?>
