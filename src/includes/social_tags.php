<!-- Twitter Cards -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="<?php echo $actual_link; ?>">
<meta name="twitter:creator" content="@Point_Locker">
<meta name="twitter:title" content="<?php echo $title; ?>">
<meta name="twitter:description" content="<?php echo $description; ?>">
<meta name="twitter:image" content="<?php echo $socialImg; ?>">

<!-- Facebook -->
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $actual_link; ?>" />
<meta property="og:image:width" content="1488" />
<meta property="og:image:height" content="701" />
<meta property="og:image" content="<?php echo $socialImg; ?>" />
<meta property="og:site_name" content="Point Locker" />

<!-- Pinterest Pins (Product) [OPENGRAPH - filling missing from FB] -->
<meta property="og:description" content="<?php echo $description; ?>" />
