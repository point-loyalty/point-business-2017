<?php
$arr = array(
    'properties' => array(
        array(
            'property' => 'email',
            'value' => $form['email']
        ),
        array(
            'property' => 'firstname',
            'value' => $form['first_name']
        ),
        array(
            'property' => 'lastname',
            'value' => $form['last_name']
        ),
        array(
            'property' => 'company',
            'value' => $form['company']
        ),
        array(
            'property' => 'phone',
            'value' => $form['phone']
        ),
        array(
            'property' => 'lifecyclestage',
            'value' => 'opportunity'
        ),
        array(
            'property' => 'hs_lead_status',
            'value' => 'NEW'
        ),
        array(
            'property' => 'lead_source',
            'value' => 'DIRECT_ONLINE_FORM'
        )
    )
);

  $post_json = json_encode($arr);
  $hapikey = "cd47ae41-beb4-4bb0-b740-ed1f52f3ae76";

    // Endpoint for simple creation
  //$endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;
    // Endpoint to check and update contacts
  $endpoint = 'https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/' . $form['email'] . '/?hapikey=' . $hapikey;

  $ch = @curl_init();
  @curl_setopt($ch, CURLOPT_POST, true);
  @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
  @curl_setopt($ch, CURLOPT_URL, $endpoint);
  @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = @curl_exec($ch);
  $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
  $curl_errors = curl_error($ch);
  @curl_close($ch);
  //echo "curl Errors: " . $curl_errors;
  //echo "\nStatus code: " . $status_code;
  //echo "\nResponse: " . $response;
