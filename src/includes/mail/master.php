<?php
$root = $_SERVER['DOCUMENT_ROOT'];
date_default_timezone_set('Europe/London');

if(isset($_POST['g-recaptcha-response']))
  $captcha=$_POST['g-recaptcha-response'];

  if(!$captcha){
    echo 'Please check the the captcha form and reload the page!';
    exit;
  }
  $secret = "6Le64yYTAAAAADLXqaNz5SwTFOFG277Hq1UDPWlD";
  $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
if($response['success'] == false) {
  echo 'You are spammer ! Get the @$%K out';
} else {

      // FINE - Let's Go!


  if (isset(
    $_POST["first_name"],
    $_POST["last_name"],
    $_POST["email"],
    $_POST["company"],
    $_POST["phone"]
  )) {


    require_once $root . '/includes/mail/PHPMailerAutoload.php';

    $form = [
      'first_name'  => strip_tags(ucwords($_POST["first_name"])),
      'last_name'   => strip_tags(ucwords($_POST["last_name"])),
      'email'       => strip_tags($_POST["email"]),
      'company'     => strip_tags(ucwords($_POST["company"])),
      'phone'       => strip_tags($_POST["phone"]),
      'ip'          => $_SERVER['REMOTE_ADDR'],
      'agent'       => strip_tags($_SERVER['HTTP_USER_AGENT']),
      'time'        => date('Y-m-d H:i:s'),
      'name'        => strip_tags(ucwords($_POST["first_name"])) . ' ' . strip_tags(ucwords($_POST["last_name"]))
    ];


    ob_start();
    include $root . '/includes/mail/content.tpl'; //execute the file as php
    $body = ob_get_clean();

    // Start mail submission session
    $mail = new PHPMailer;
    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use) // 1 = client messages // 2 = client and server messages
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';

    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;

    // SMTP Login Credentials
    $mail->Username = "luke@point-locker.com";
    $mail->Password = "Cotty5853";

    $mail->setFrom($form["email"], $form["name"]); //Set who the message is to be sent from
    $mail->addReplyTo($form["email"], $form["name"]); //Set an alternative reply-to address
    $mail->addAddress('luke@point-locker.com', 'Point Locker'); //Set who the message is to be sent to

    $mail->Subject = $form["company"] . ' - Point Locker Business Enquiry';
    $mail->MsgHTML($body);
    //Alternative Body Text
    $mail->AltBody = $form["name"] . ", " . $form["email"] . ", " . $form["company"] . ", " . $form["phone"];


    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
        $error_message = $mail->ErrorInfo;
    } else { // If successful
        echo $form["name"] . ', we heard you loud and clear! We\'ll be in touch shortly.';
        // Log into Hubspot CRM
          include_once $root . '/includes/mail/hubspot.php';
    }

  } else {
    echo ("It broke! Sorry :O Maybe try again?");
  };
} // Google recaptcha

?>
