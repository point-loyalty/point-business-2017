<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css" id="">
		a {
			word-break: break-word;
		}

		a img {
			border: none;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}

		body {
			width: 100% !important;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: 100%;
		}

		#page-wrap {
			margin: 0;
			padding: 0;
			width: 100% !important;
			line-height: 100% !important;
		}

		#outlook a {
			padding: 0;
		}

		.preheader {
			display: none !important;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		.a5q {
			display: none !important;
		}
	</style>
	<style type="text/css" id="">
		@media only screen and (max-width: 580px) {
			table[class=email-body-wrap] {
				width: 320px !important;
			}
			td[class=page-bg-show-thru] {
				display: none !important;
			}
			table[class=layout-block-wrapping-table] {
				width: 320px !important;
			}
			table[class=mso-fixed-width-wrapping-table] {
				width: 320px !important;
			}
			*[class=layout-block-full-width] {
				width: 320px !important;
			}
			table[class=layout-block-column],
			table[class=layout-block-padded-column] {
				width: 100% !important;
			}
			table[class=layout-block-box-padding] {
				width: 100% !important;
				padding: 5px !important;
			}
			table[class=layout-block-horizontal-spacer] {
				display: none !important;
			}
			tr[class=layout-block-vertical-spacer] {
				display: block !important;
				height: 8px !important;
			}
			td[class=container-padding] {
				display: none !important;
			}
			table {
				min-width: initial !important;
			}
			td {
				min-width: initial !important;
			}
			*[class~=desktop-only] {
				display: none !important;
			}
			*[class~=mobile-only] {
				display: block !important;
			}
			.hide {
				max-height: none !important;
				display: block !important;
				overflow: visible !important;
			}
			div[id="EQ-00"] [class~="layout-block-padding-left"] {
				width: 12px !important;
			}
			div[id="EQ-00"] [class~="layout-block-padding-right"] {
				width: 12px !important;
			}
			div[id="EQ-00"] [class~="layout-block-content-cell"] {
				width: 296px !important;
			}
			td[id="EQ-01"] {
				height: 10px !important;
			}
			/* vertical spacer */
			div[id="EQ-02"] [class~="layout-block-padding-left"] {
				width: 12px !important;
			}
			div[id="EQ-02"] [class~="layout-block-padding-right"] {
				width: 12px !important;
			}
			div[id="EQ-02"] [class~="layout-block-content-cell"] {
				width: 296px !important;
			}
			td[id="EQ-03"] {
				height: 10px !important;
			}
			/* vertical spacer */
			div[id="EQ-04"] [class~="layout-block-padding-left"] {
				width: 12px !important;
			}
			div[id="EQ-04"] [class~="layout-block-padding-right"] {
				width: 12px !important;
			}
			div[id="EQ-04"] [class~="layout-block-content-cell"] {
				width: 296px !important;
			}
		}
	</style>
	<!--[if gte mso 9]>
	<style type="text/css" id="Mail Designer Outlook Style Sheet">
		table.layout-block-horizontal-spacer {
		    display: none !important;
		}
		table {
		    border-collapse:collapse;
		    mso-table-lspace:0pt;
		    mso-table-rspace:0pt;
		    mso-table-bspace:0pt;
		    mso-table-tspace:0pt;
		    mso-padding-alt:0;
		    mso-table-top:0;
		    mso-table-wrap:around;
		}
		td {
		    border-collapse:collapse;
		    mso-cellspacing:0;
		}
	</style>
	<![endif]-->
	<link href="http://fonts.googleapis.com/css?family=Raleway:regular,700" rel="stylesheet" type="text/css" class="EQWebFont">
	<link href="http://fonts.googleapis.com/css?family=Droid+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
	<link href="http://fonts.googleapis.com/css?family=Roboto:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " background="http://sugarskulluk.com/point-email-assets/business-enquiries/page-bg.jpg">
	<!--[if gte mso 9]>
<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
<v:fill type="tile" src="http://sugarskulluk.com/point-email-assets/business-enquiries/page-bg.jpg" />
</v:background>
<![endif]-->

	<table width="100%" cellspacing="0" cellpadding="0" id="page-wrap" align="center" background="http://sugarskulluk.com/point-email-assets/business-enquiries/page-bg.jpg">
		<tbody>
			<tr>
				<td>

					<table class="email-body-wrap" width="700" cellspacing="0" cellpadding="0" id="email-body" align="center">
						<tbody>
							<tr>
								<td width="30" class="page-bg-show-thru">&nbsp;
									<!--Left page bg show-thru -->
								</td>
								<td width="640" id="page-body">

									<!--Begin of layout container -->
									<div id="eqLayoutContainer">

										<div width="100%" class="desktop-only">
											<table width="640" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table" style="min-width: 640px; ">
												<tbody>
													<tr>
														<td valign="top" class="layout-block-full-width" width="640" style="min-width: 640px; ">
															<table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="640" style="min-width: 640px; ">
																<tbody>
																	<tr>
																		<td width="640" style="min-width: 640px; ">
																			<div class="layout-block-image">
																				<!--[if mso]><img width="640" height="120" alt=""  src="http://sugarskulluk.com/point-email-assets/business-enquiries/image-3.png" border="0" style="line-height:1px;"><![endif]-->
																				<!--[if !mso]><!----><img width="640" height="120" alt="" src="http://sugarskulluk.com/point-email-assets/business-enquiries/image-3.png" border="0" style="line-height: 1px; display: block; ">
																				<!--<![endif]-->
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden; ">
											<table width="320" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; min-width: 320px; ">
												<tbody>
													<tr>
														<td valign="top" class="layout-block-full-width" width="320" style="min-width: 320px; ">
															<table cellspacing="0" cellpadding="0" class="layout-block-full-width hide" width="320" style="display: none; max-height: 0px; overflow: hidden; min-width: 320px; ">
																<tbody>
																	<tr>
																		<td width="320" style="min-width: 320px; ">
																			<div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden; ">
																				<!--[if mso]><img width="640" height="154" alt="" src="http://sugarskulluk.com/point-email-assets/business-enquiries/image-3.png" border="0" style="line-height:1px;" id="EQMST-01AF84F7-2E5C-479A-A098-38BC85022998"><![endif]--><img width="320" height="77" alt="" src="http://sugarskulluk.com/point-email-assets/business-enquiries/image-1.png" border="0" style="line-height: 1px; display: none; max-height: 0px; overflow: hidden; " class="hide">
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" id="EQ-00">
											<table width="640" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 640px; ">
												<tbody>
													<tr>
														<td width="24" class="layout-block-padding-left" style="font-size: 1px; min-width: 24px; ">&nbsp;</td>
														<td height="20" class="layout-block-content-cell" width="592" style="min-width: 592px; " id="EQ-01">
															<div class="spacer"></div>
														</td>
														<td width="24" class="layout-block-padding-right" style="font-size: 1px; min-width: 24px; ">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" class="desktop-only">
											<table width="640" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 640px; ">
												<tbody>
													<tr>
														<td width="24" class="layout-block-padding-left" style="min-width: 24px; ">&nbsp;</td>
														<td valign="top" align="left" class="layout-block-content-cell" width="592" style="min-width: 592px; ">
															<table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="592" style="min-width: 592px; ">
																<tbody>
																	<tr>
																		<td width="572" valign="top" align="left" style="padding-left: 10px; padding-right: 10px; min-width: 572px; ">
																			<table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="572" style="min-width: 572px; ">
																				<tbody>
																					<tr>
																						<td align="left" class="layout-block-column" width="572" style="min-width: 572px; ">
																							<div class="heading" style="font-size: 16px; font-family: 'Lucida Grande'; text-align: center; "><span style="font-family: Raleway, Arial, sans-serif; font-size: 27px; ">Enquiry - <?php echo $form["company"]; ?></span>
																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td width="24" class="layout-block-padding-right" style="min-width: 24px; ">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden; ">
											<table width="320" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden; min-width: 320px; ">
												<tbody>
													<tr>
														<td width="12" class="layout-block-padding-left" style="min-width: 12px; ">&nbsp;</td>
														<td valign="top" align="left" class="layout-block-content-cell" width="296" style="min-width: 296px; ">
															<table cellspacing="0" cellpadding="0" align="left" class="layout-block-column hide" width="296" style="display: none; max-height: 0px; overflow: hidden; min-width: 296px; ">
																<tbody>
																	<tr>
																		<td width="276" valign="top" align="left" style="padding-left: 10px; padding-right: 10px; min-width: 276px; ">
																			<table cellspacing="0" cellpadding="0" class="layout-block-box-padding hide" width="276" style="display: none; max-height: 0px; overflow: hidden; min-width: 276px; ">
																				<tbody>
																					<tr>
																						<td align="left" class="layout-block-column" width="276" style="min-width: 276px; ">
																							<div class="heading" style="font-size: 16px; font-family: 'Lucida Grande'; text-align: center; "><span style="font-family: Raleway, Arial, sans-serif; font-size: 26px; ">Enquiry - <?php echo $form["company"]; ?></span>
																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td width="12" class="layout-block-padding-right" style="min-width: 12px; ">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" id="EQ-02">
											<table width="640" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 640px; ">
												<tbody>
													<tr>
														<td width="24" class="layout-block-padding-left" style="font-size: 1px; min-width: 24px; ">&nbsp;</td>
														<td height="20" class="layout-block-content-cell" width="592" style="min-width: 592px; " id="EQ-03">
															<div class="spacer"></div>
														</td>
														<td width="24" class="layout-block-padding-right" style="font-size: 1px; min-width: 24px; ">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div width="100%" id="EQ-04">
											<table width="640" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 640px; ">
												<tbody>
													<tr>
														<td width="24" class="layout-block-padding-left" style="min-width: 24px; ">&nbsp;</td>
														<td class="layout-block-content-cell" width="592" style="min-width: 592px; ">
															<table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="592" style="min-width: 592px; ">
																<tbody>
																	<tr>
																		<td width="572" valign="top" align="left" style="padding-left: 10px; padding-right: 10px; min-width: 572px; ">
																			<table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="572" style="min-width: 572px; ">
																				<tbody>
																					<tr>
																						<td align="left" class="layout-block-column" width="572" style="min-width: 572px; ">
																							<div class="text" style="font-size: 16px; font-family: 'Lucida Grande'; ">
																								<div style="text-align: center; "><span style="font-size: 15px; "><span style="font-family: Raleway, Arial, sans-serif; ">New online enquiry received on: </span><b style="font-family: Raleway, Arial, sans-serif; "><br><?php echo $form["time"]; ?></b></span>
																								</div>
																								<div style="line-height: 1.5; ">
																									<font style="line-height: 1.5; " face="Raleway, Arial, sans-serif"><b style="font-size: 15px; "><br></b></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif"><b><u style="font-size: 15px; ">Client Information</u></b></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; ">Name: <?php echo $form["name"]; ?></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; ">Email: <a href="mailto:<?php echo $form["email"]; ?>"><?php echo $form["email"]; ?></a></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; ">Company: <?php echo $form["company"]; ?></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; ">Phone: <a href="tel:<?php echo $form["phone"]; ?>"><?php echo $form["phone"]; ?></a></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; "><br></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; ">IP: <?php echo $form["ip"]; ?></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif" style="font-size: 15px; "><?php echo $form["agent"]; ?></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif"><span style="font-size: 14px; "><br></span></font>
																								</div>
																								<div>
																									<font face="Raleway, Arial, sans-serif"><span style="font-size: 14px; "><br></span></font>
																								</div>
																							</div>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td width="24" class="layout-block-padding-right" style="min-width: 24px; ">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>

									</div>
									<!--End of layout container -->

								</td>
								<td width="30" class="page-bg-show-thru">&nbsp;
									<!--Right page bg show-thru -->
								</td>
							</tr>
						</tbody>
					</table>
					<!--email-body -->

				</td>
			</tr>
		</tbody>
	</table>
	<!--page-wrap -->


</body>

</html>
