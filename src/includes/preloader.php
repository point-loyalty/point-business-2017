<!-- Preloader -->
<div id="preloader">
  <div id="status">
    <div class="table-cell">
    <img src="img/brand_logo_white.png" /><br /><br />
    <div class="browser-screen-loading-content">
      <div class="loading-dots dark-gray">
        <i></i>
        <i></i>
        <i></i>
        <i></i>
      </div>
    </div>
  </div>
  </div>
</div>
