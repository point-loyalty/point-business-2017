<div class="top-bar">
  <div class="top-bar-title">
    <?php if ($responsiveMenu === TRUE) { ?>
      <span class="menu-icon-span" data-responsive-toggle="responsive-menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle></button>
      </span>
    <?php } ?>
    <img src="img/logo-alt.png" />
  </div>
  <?php if ($responsiveMenu === TRUE) { ?>
  <div id="responsive-menu">
    <div class="top-bar-right">
      <ul class="header-menu">
        <li><a href="#">Near You <i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
        <li class="active"><a href="#">Blog</a></li>
        <li><a href="#">Discover</a></li>
        <li><a href="#">Login</a></li>
        <li class="feature"><a href="#">Join Us</a></li>
      </ul>
    </div>
  </div>
  <?php } ?>
</div>
