<script type="text/javascript">
  var onloadCallback = function() {
    grecaptcha.render('captcha', {
      'sitekey' : '6Le64yYTAAAAAOaNH7XIEDdiB7tJ696TYSZB0E25',
      'callback' : recaptchaCallback
    });
  };
  function recaptchaCallback() {
      $('#form_send').removeAttr('disabled');
      $('button#form_send').text('Submit');
  };
</script>



<div class="remodal form-modal" data-remodal-id="enquire">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="container-full">

    <div id="modal-header" class="row align-center">
      <div class="small-10 medium-6">
        <img src="img/thelockerNL.png" class="modal-header-img" alt="The Locker" />
        <p>Complete the form below to start building your customer reach.</p>
      </div>
    </div>

    <div id="form-response" class="row">
      <div class="column small-12">
        <div id="form-message" class="response valid"></div>
      </div>
    </div>

    <form id="enquiry-form" data-focus="false" data-abide novalidate>
    <div class="row">
      <div class="column small-12 medium-6 form-left">
        <?php formItem(NULL, 'first_name', 'First Name', 'Enter your First name', 'text', TRUE, NULL, TRUE, 128); ?>
      </div>
      <div class="column small-12 medium-6 form-right">
        <?php formItem(NULL, 'last_name', 'Last Name', 'Enter your Last Name', 'text', TRUE, NULL, TRUE, 128); ?>
      </div>
    </div>

    <div class="row split-form">
      <div class="column small-12 medium-6 form-left">
        <?php formItem(NULL, 'email', 'Email Address', 'Enter your Email Address', 'email', TRUE, NULL, NULL, NULL); ?>
      </div>
      <div class="column small-12 medium-6 form-right">
        <?php formItem(NULL, 'company', 'Company', 'Enter your Company Name', 'text', TRUE, NULL, NULL, NULL); ?>
      </div>
    </div>

    <div class="row">
      <div class="column small-12">
        <?php formItem(NULL, 'phone', 'Contact Number', 'Enter your Company Number', 'tel', TRUE, NULL, NULL, NULL); ?>
      </div>
    </div>

    <!-- ••• RECAPTCHA ••• -->
    <div class="row align-center">
      <div id="captcha-align" class="column small-12">
        <div class="g-recaptcha" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;" id="captcha"></div>
      </div>
    </div>

    <div class="row">
      <div class="column small-12">
        <button disabled type="submit" value="submit" id="form_send" name="contactSubmit" class="button feature wide">Complete the form</button>
      </div>
    </div>

  </form>
  <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
  async defer></script>

  </div>

</div>
