<?php
  // SEO Dynamic Information
  $title = "Point Locker - Campaign Submission / Update";
  $description = "";
  $socialImg = "http://www.point-locker.com/img/social-card.jpg";
  list($width, $height) = getimagesize($socialImg);
  $actual_link = "";


  $robots = FALSE;

  // Forms Config
  $forms = TRUE;
  ($forms === TRUE) ? include 'includes/form_functions.php' : '' ;
  // HELP
  // formItem(NULL, $id, $labelText, $placeholder, $type, $required, $error, $count, $max);
  // formTextarea($label, $id, $labelText, $required, $error, $count, $max)

 ?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <?php include_once ('includes/favicon.php'); ?>
    <!-- Social SEO -->
    <?php include_once ('includes/social_tags.php'); ?>
    <!-- Meta Tags -->
    <meta name="application-name" content="<?php echo $title; ?>"/>
    <meta name="description" content="<?php echo $description; ?>" />
    <meta name="robots" content="<?php if ($robots === FALSE) { echo ("noindex"); } else { echo ("index"); } ?>" />
    <title><?php echo $title; ?></title>

    <!-- Stylesheets -->
    <script src="https://use.fontawesome.com/11cd6defb5.js"></script>
    <link rel="stylesheet" href="css/core.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/modernizr.js"></script>
  </head>

  <body>

  <!-- Common header -->
  <?php
  $responsiveMenu = FALSE;
  $stickyMenu = FALSE;
  include 'includes/menu.php';
  ?>


    <div style="margin-top: 40px;" id="header" class="container-full">


      <div style="margin-bottom: 0px;" class="row">
        <div id="form" class="small-12 medium-6 column">
            <form data-abide novalidate>

              <div data-abide-error class="alert callout" style="">
                <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> There are some errors in your form.</p>
              </div>

              <div class="row">
                <div class="small-12 medium-6 column">
                  <?php formItem(NULL, 'company', 'Company (Autofill)', 'Enter your Company Name', 'text', TRUE, NULL, NULL, NULL); ?>
                </div>
                <div class="small-12 medium-6 column">
                  <?php formItem(NULL, 'firstname', 'First Name', 'Enter your First Name', 'text', TRUE, NULL, TRUE, 55); ?>
                </div>
              </div>
              <div class="row">
                <div class="small-12 column">
                  <div class="input-field">
                  <label for="selection">Select Menu</label>
                    <select id="selection">
                      <option value="husker">Husker</option>
                      <option value="starbuck">Starbuck</option>
                      <option value="hotdog">Hot Dog</option>
                      <option value="apollo">Apollo</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="small-12 column">
                  <?php formTextarea(NULL, 'terms', 'Terms & Conditions', FALSE, NULL, TRUE, 225); ?>
                </div>
              </div>

              <div class="row">
                <div class="small-12 column searchable-container">
                  <div class="input-field">
                    <label for="company">Search the Dropdown</label>
                    <select id="company" class="js-example-responsive searchable" style="width: 100%">
                      <option value="husker">Luke</option>
                      <option value="starbuck">Joro</option>
                      <option value="hotdog">Alexander Joshua</option>
                      <option value="apollo">Point Locker</option>
                      <option value="what">CAST Sheffield</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="small-12 column">
                  <!-- • GENDER -->
                  <div class="row gender">
                    <div class="small-6 column gender-item">
                      <input type="radio" name="radio" id="male" class="radio" checked/>
                      <label class="radio_opt" for="male">Male</label>
                    </div>
                    <div class="small-6 column gender-item">
                      <input type="radio" name="radio" id="female" class="radio"/>
                      <label class="radio_opt" for="female">Female</label>
                    </div>
                  </div>
                </div>
              </div>

            </form>
        </div>

        <div id="form" class="small-12 medium-6 column">
          <form data-abide novalidate>
          <div class="row">
            <div class="small-12 column">
              <div class="input-field nolabel">
                <label for="dob">Birthday</label>
                <input class="date" type="date" id="dob" class="form-field" required placeholder="dd/mm/yyyy"/>
                  <span class="bar"></span>

                <span class="form-error">
                  This is required
                </span>

              </div>
            </div>
          </div>
          </form>

          <div class="row">
            <div class="small-12 column">
              <?php formItem(TRUE, 'nolabel', 'No Label Field', 'There is no inner-label here', 'number', TRUE, NULL, NULL, ''); ?>
            </div>
          </div>

          <div class="row">
            <div class="small-12 column">
              <div class="input-field nolabel">
                <label for="email">First Name</label>
                <input id="email" type="text" aria-describedby="exampleHelpText" required pattern="text" />
                <span class="bar"></span>
                <span class="form-error">
                  This is required
                </span>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="small-12 column">
              <div class="input-field">
                <label for="company">First Name</label>
                <input id="company" type="text" aria-describedby="exampleHelpText" required pattern="text" />
                <span class="bar"></span>
                <span class="form-error">
                  This is required
                </span>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="small-12 column">
              <?php formTextarea(NULL, 'comments', 'Any Comments?', TRUE, NULL, FALSE, ''); ?>
            </div>
          </div>

        </div>
      </div>







      <div class="row">
        <div class="small-12 medium-6 column">
          <div class="row">
            <div class="small-12 medium-6 column">
              <h1>Hello, World!</h1>
              <h2>Hello, World!</h2>
              <h3>Hello, World!</h3>
              <h4>Hello, World!</h4>
              <h5>Hello, World!</h5>
              <h6>Hello, World!</h6>
            </div>
            <div class="small-12 medium-6 column">
              <h1 class="subheader">Hello, World!</h1>
              <h2 class="subheader">Hello, World!</h2>
              <h3 class="subheader">Hello, World!</h3>
              <h4 class="subheader">Hello, World!</h4>
              <h5 class="subheader">Hello, World!</h5>
              <h6 class="subheader">Hello, World!</h6>
            </div>
          </div>


          <br /><br />

          <div class="row">
            <div class="small-12 medium-6 column">
              <h5>.Feature Button</h5>
              <a href="#" class="button feature">Feature Button</a><br />
              <a href="#" class="button feature disabled" disabled>Disabled Button</a><br />
              <a href="#" class="button feature wide">Button [.wide]</a><br />
            </div>
            <div class="small-12 medium-6 column">
              <h5>.secondary Button</h5>
              <button href="#" class="button secondary">Secondary Button</button><br />
              <button href="#" class="button secondary disabled" disabled>Disabled Button</button><br />
              <button href="#" class="button secondary wide">Button [.wide]</button><br />
            </div>
          </div>
          <br /><br />
          <div class="row">
            <div class="small-12 medium-6 column">
              <h5>.outline Feature</h5>
              <a href="#" class="button feature_outline">Feature Button</a><br />
              <a href="#" class="button feature_outline" disabled>Disabled Button</a><br />
              <a href="#" class="button feature_outline wide">Button [.wide]</a><br />
            </div>
            <div class="small-12 medium-6 column">
              <h5>.outline Secondary</h5>
              <a href="#" class="button light_outline">Feature Button</a><br />
              <a href="#" class="button light_outline" disabled>Disabled Button</a><br />
              <a href="#" class="button light_outline wide">Button [.wide]</a><br />
            </div>
          </div>

        </div>
        <div class="small-12 medium-6 column">
          <h3>Typography Styles</h3>
          <h5>Introduction Text <small>.intro</small></h5>
          <p class="intro">
            <strong>Lorem ipsum dolor sit amet</strong>, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation ullamco laboris</a> nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <br />

          <h5>Default Text <small>N/A</small></h5>
          <p>
            <strong>Lorem ipsum dolor sit amet</strong>, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation ullamco laboris</a> nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <br />

          <h5>Small Text <small>.small</small></h5>
          <p class="small">
            <strong>Lorem ipsum dolor sit amet</strong>, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation ullamco laboris</a> nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <br />


          <h5>Legal or MINI Text <small>.mini</small></h5>
          <p class="mini">
            <strong>Lorem ipsum dolor sit amet</strong>, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <a href="#">quis nostrud exercitation ullamco laboris</a> nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
          <br />

          <div style="margin-bottom:30px;"></div>



        </div>
      </div>

    </div>



    <script src="js/core.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      // Autocomplete INIT
      $('input#company').autoComplete({
        minChars: 1,
        source: function(term, suggest){
          term = term.toLowerCase();
          var choices = [
            'Upshot Espresso',
            'Kelham Island Brewery',
            'Joro',
            'Depot Bakery',
            'Tampa Coffee',
            'Alexander Joshua',
            'CAST'
          ];
          var suggestions = [];
          for (i=0;i<choices.length;i++)
              if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
          suggest(suggestions);
        }
      });

      // SEARCHABLE CONTAINER INIT
      $(".js-example-responsive").select2();


      // Character Count INIT
      // Function file is in includes.js
      function countInit(field, count) {
        var counter = $('#chars-' + field);
        $('#' + field).limiter(count, counter);
      }
      countInit('firstname', 55); // First Name
      countInit('terms', 255); // Terms

    });

    </script>
  </body>
</html>
