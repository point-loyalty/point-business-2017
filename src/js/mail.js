// Master chain
$('#enquiry-form')
  .on("invalid.zf.abide", function(ev,elem) {
    console.log("Field id "+ev.target.id+" is invalid");
  })
  .on("formvalid.zf.abide", function(ev,frm) {
    // If the form is valid on submission, it will fire!
    // Trigger visual functions and ajax request

    toggleButton();
    $.ajax({
      type: 'POST',
      url: "/includes/mail/master.php",
      data: $('#enquiry-form').serialize(),
      success : function(response) {
        formSuccess(response);
        submitMSG(true, response);
      },
      error : function(response) {
        console.log("It all fucked up");
        submitMSG(false, response);
      }
    });
  })
  // to prevent form from submitting upon successful validation
  .on("submit", function(ev) {
    ev.preventDefault();
    console.log("Submit for form id "+ev.target.id+" intercepted");
  });



  // Toggle text on submit
  function toggleButton() {
    var button = $('button#form_send');
    button.addClass(' disabled ').attr('disabled');
    button.text('Please wait...')
  }

  function submitMSG(valid, msg) {
    var msgClasses;
    if (valid === true) {
      msgClasses = 'response valid animated fadeIn';
    } else {
      msgClasses = 'response error animated fadeIn';
    }
    //$('#form-response').css('display', 'inherit');
    $('#form-message').removeClass().addClass(msgClasses).text(msg);
  }

  // Successful form submit
  function formSuccess(response) {
      $('#enquiry-form')[0].reset();
      $('button#form_send').fadeOut();
      $('#enquiry-form').fadeOut();
      $('#modal-header p').fadeOut();

      $('#form-response').fadeIn();
  }
