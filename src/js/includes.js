// Foundation init
$(document).foundation();


$(document).ready(function() {
  // REQUIRED FOR DYNAMIC FORM LABELS
  // Dynamic form label
  $('.input-field.nolabel input, .input-field.nolabel textarea').each(function() {

    $(this).on('focus', function(){
      $(this).parent('.input-field.nolabel').addClass('active');
    });

    $(this).on('blur', function() {
      if ($(this).val().length === 0) {
        $(this).parent('.input-field.nolabel').removeClass('active');
      }
    });

    if ($(this).val() !== '') {
      $(this).parent('.input-field.nolabel').addClass('active');
    }
  });


  //jQuery Input counter :: START
  (function($) {
  	$.fn.extend( {
  		limiter: function(limit, elem) {
  			$(this).on("keyup focus", function() {
  				setCount(this, elem);
  			});
  			function setCount(src, elem) {
  				var chars = src.value.length;
  				if (chars > limit) {
  					src.value = src.value.substr(0, limit);
  					chars = limit;
  				}
  				elem.html( limit - chars + " / " + limit );
  			}
  			setCount($(this)[0], elem);
  		}
  	});
  })(jQuery);
  //jQuery Input counter :: END
  //////////////////////////////

});
