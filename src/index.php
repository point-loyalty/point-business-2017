<?php
  // SEO Dynamic Information
  $title = "";
  $description = "";

  $socialImg = "http://www.point-locker.com/img/social-card.jpg";
  list($width, $height) = getimagesize($socialImg);

  $actual_link = "";

  $robots = TRUE;

 ?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <?php include_once ('includes/favicon.php'); ?>
    <!-- Social SEO -->
    <?php include_once ('includes/social_tags.php'); ?>
    <!-- Meta Tags -->
    <meta name="application-name" content="<?php echo $title; ?>"/>
    <meta name="description" content="<?php echo $description; ?>" />
    <meta name="robots" content="<?php if ($robots = FALSE) { echo ("noindex"); } else { echo ("index"); } ?>" />
    <title><?php echo $title; ?></title>

    <!-- Stylesheets -->
    <script src="https://use.fontawesome.com/11cd6defb5.js"></script>
    <link rel="stylesheet" href="css/core.min.css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/modernizr.js"></script>
  </head>

  <body>

    <!-- Common header -->
    <?php
    $responsiveMenu = FALSE;
    $fixed = TRUE;
    include 'includes/menu.php';
    ?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/core.js"></script>
  </body>
</html>
